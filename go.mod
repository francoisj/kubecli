module gitlab.com/radiofrance/kubecli

go 1.12

require (
	k8s.io/api v0.0.0-20190409092523-d687e77c8ae9
	k8s.io/apimachinery v0.0.0-20190409092423-760d1845f48b
	k8s.io/client-go v0.0.0-20190409092706-ca8df85b1798
)
