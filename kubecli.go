package kubecli

import (
	"fmt"
	"os"
	"path/filepath"

	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
	"k8s.io/client-go/tools/clientcmd"
)

// KubeCli wraps k8s.io clientSet
type KubeCli struct {
	ClientSet *kubernetes.Clientset
	Namespace string
}

// New creates a new KubeCli client from Kubeconfig
func New(context string) (*KubeCli, error) {

	var config *rest.Config
	var namespace string
	var err, err2 error

	config, err = inClusterConfig()
	if err != nil {
		// Maybe we are not "in cluster" but "out of" cluster, let's try this
		config, namespace, err2 = outOfClusterConfig(context)
		if err2 != nil {
			return nil, fmt.Errorf("could not create Kube Configuration either inCluster or outOfCluster, possible reasons: [%v, %v]", err, err2)
		}
	}

	clientSet, err := kubernetes.NewForConfig(config)
	if err != nil {
		return nil, fmt.Errorf("could not create ClientSet: %v", err)
	}

	return &KubeCli{
		ClientSet: clientSet,
		Namespace: namespace,
	}, nil
}

// NewOutOfCluster creates a KubeCli instance using only "out-of-cluster" configuration
func NewOutOfCluster(context string) (*KubeCli, error) {
	config, namespace, err := outOfClusterConfig(context)
	if err != nil {
		return nil, fmt.Errorf("could not create Kube Configuration, reason: %v", err)
	}

	clientSet, err := kubernetes.NewForConfig(config)
	if err != nil {
		return nil, fmt.Errorf("could not create ClientSet: %v", err)
	}

	return &KubeCli{
		ClientSet: clientSet,
		Namespace: namespace,
	}, nil
}

func outOfClusterConfig(context string) (*rest.Config, string, error) {
	kubeconfig := os.Getenv("KUBECONFIG")
	if kubeconfig == "" {
		kubeconfig = filepath.Join(os.Getenv("HOME"), ".kube", "config")
	}

	override := &clientcmd.ConfigOverrides{}
	if context != "" {
		override.CurrentContext = context
	}

	config := clientcmd.NewNonInteractiveDeferredLoadingClientConfig(
		&clientcmd.ClientConfigLoadingRules{ExplicitPath: kubeconfig}, override)
	clientConfig, err := config.ClientConfig()
	if err != nil {
		return nil, "", err
	}

	namespace, _, err := config.Namespace()
	if err != nil {
		return nil, "", err
	}

	return clientConfig, namespace, nil
}

func inClusterConfig() (*rest.Config, error) {
	return rest.InClusterConfig()
}
